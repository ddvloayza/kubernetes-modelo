import json
from django.shortcuts import render
from django.http.response import JsonResponse, HttpResponse
from django.views.generic import TemplateView, ListView
from proyecto.apps.blog.models import BlogArticle, CategoryArticle, BlogTag, ConfigBlog
from django.shortcuts import get_object_or_404


class HomeView(TemplateView):

    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        lists_articles = BlogArticle.objects.all()

        context.update(
            {
                "lists_articles": lists_articles,
            }
        )
        return context

class ArticleDetailView(TemplateView):

    template_name = 'blog_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        article = get_object_or_404(BlogArticle, slug=self.kwargs['slug'])
        list_categorias = CategoryArticle.objects.all()
        list_tags = BlogTag.objects.all()

        context.update(
            {
                "article": article,
                "list_categorias": list_categorias,
                "list_tags": list_tags
            }
        )
        return context

class ArticleListView(ListView):

    paginate_by = 9
    template_name = 'blog_list.html'

    def get_queryset(self):
        queryset = BlogArticle.objects.all() 
        return queryset

        
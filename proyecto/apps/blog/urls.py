from django.urls import path
from .views import HomeView, ArticleDetailView, ArticleListView


app_name = 'blog'

urlpatterns = [
	path('', HomeView.as_view(), name='home'),

    path('<slug:slug>/articulo/',
         ArticleDetailView.as_view(),
         name='article_details'),

    path('listado/articulos/',
         ArticleListView.as_view(),
         name='article_list'),


]

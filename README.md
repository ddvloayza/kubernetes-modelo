# KUBERNETES
Sample Proyect

## Requirements
- Kubernetes
    ```
    https://kubernetes.io/docs/tasks/tools/install-kubectl/
    ```
- Minikube
    ```
    https://kubernetes.io/es/docs/tasks/tools/install-minikube/
    ```
- Docker
    ```
    https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04-es
    ```
